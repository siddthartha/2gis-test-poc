FROM phpswoole/swoole:5.0-php8.2-alpine

WORKDIR /var/www

COPY ./src .

RUN composer install -o

EXPOSE 80

CMD ["php", "index.php"]

