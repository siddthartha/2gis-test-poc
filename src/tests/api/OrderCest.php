<?php

use Codeception\Util\HttpCode;
use Tests\Support\ApiTester;

class OrderCest
{
    public function _before(ApiTester $I)
    {
    }

    public function createOrderSuccessfully(ApiTester $I)
    {
        $I->wantTo('create an order successfully');

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/orders', [
            'hotel_id' => 'reddison',
            'room_id' => 'lux',
            'email' => 'test@example.com',
            'from' => '2024-10-01',
            'to' => '2024-10-03'
        ]);

        $I->seeResponseCodeIs(HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'hotel_id' => 'reddison',
            'room_id' => 'lux',
            'email' => 'test@example.com',
            'from' => '2024-10-01',
            'to' => '2024-10-03'
        ]);
    }

    public function createOrderAlreadyBooked(ApiTester $I)
    {
        $I->wantTo('try create an order with same dates');

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/orders', [
            'hotel_id' => 'reddison',
            'room_id' => 'lux',
            'email' => 'test@example.com',
            'from' => '2024-10-01',
            'to' => '2024-10-03'
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'error' => 'Hotel room is not available for selected dates'
        ]);
    }

    public function createOrderWithUnavailableDates(ApiTester $I)
    {
        $I->wantTo('fail creating an order with unavailable dates');

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/orders', [
            'hotel_id' => 'reddison',
            'room_id' => 'lux',
            'email' => 'test@example.com',
            'from' => '2024-10-05',
            'to' => '2024-10-06'
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST); // 400 -- bad request
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'error' => 'Hotel room is not available for selected dates'
        ]);
    }

    public function createOrderWithInvalidInput(ApiTester $I)
    {
        $I->wantTo('fail creating an order with invalid input');

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/orders', [
            'hotel_id' => 'reddison',
            'email' => 'test@example.com',
            'from' => '2024-10-03'
        ]);

        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY); // 422
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'error' => 'Missing required fields [room_id, to] in model App\Models\Order'
        ]);
    }

    public function createALotOfOrders(ApiTester $I)
    {
        for ($i = 0; $i < 10000; $i++)
        {
            $randomDate = rand(2025, 2030)
                . '-' . str_pad(rand(1, 12), 2, "0", STR_PAD_LEFT)
                . '-' . str_pad(rand(1, 28), 2, "0", STR_PAD_LEFT);

            $I->haveHttpHeader('Content-Type', 'application/json');
            $I->sendPOST('/orders', [
                'hotel_id' => 'reddison',
                'room_id' => 'lux',
                'email' => 'test@example.com',
                'from' => $randomDate,
                'to' => (new DateTime($randomDate))
                    ->add(new DateInterval('P' . rand(1, 14). 'D'))
                    ->format('Y-m-d'),
            ]);

            $I->comment($I->grabResponse());

            $i++;
        }
    }

}

