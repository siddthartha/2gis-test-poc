<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\Order;
use App\Models\RoomAvailability;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Table;

class OrderController
{
    const STATUS_VALIDATION_ERROR = 422;
    const STATUS_NOT_FOUND = 400;
    const STATUS_CREATED = 201;

    private Table $availability;

    public function __construct(Table &$availability)
    {
        $this->availability = &$availability;
    }

    public function createOrder(Request $request, Response $response)
    {
        $response->header("Content-Type", "application/json");


        $input = json_decode($request->rawContent(), true);

        try
        {
            $newOrder = new Order($input);
        }
        catch (\Throwable $exception)
        {
            $response->status(self::STATUS_VALIDATION_ERROR);
            $response->end(json_encode(['error' => $exception->getMessage()]));

            return;
        }

        echo "Order: " . json_encode($newOrder) . "\n";

        $availabilityService = new RoomAvailability($this->availability);

        $unavailableDays = $availabilityService->isPeriodAvailable(
            $newOrder->getAttribute('hotel_id'),
            $newOrder->getAttribute('room_id'),
            new \DateTime($newOrder->getAttribute('from')),
            new \DateTime($newOrder->getAttribute('to'))
        );


        echo "unavailable days: " . json_encode($unavailableDays) . "\n";

        if (!empty($unavailableDays))
        {
            $response->status(self::STATUS_NOT_FOUND);
            $response->end(json_encode(['error' => 'Hotel room is not available for selected dates']));

            return;
        }

        $availabilityService->bookRoom($newOrder);

        $response->status(self::STATUS_CREATED);
        $response->end(json_encode($newOrder));
    }
}

