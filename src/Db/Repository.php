<?php
declare(strict_types=1);

namespace App\Db;

use App\Models\RoomAvailability;
use Swoole\Table;

$repository = new Table(1024 * 1024); // 1Mb of shared memory

$repository->column('hotel_id', Table::TYPE_STRING, 64);
$repository->column('room_id', Table::TYPE_STRING, 64);
$repository->column('date', Table::TYPE_STRING, 32);

$repository->create();

foreach ([
    ['hotel_id' => 'reddison', 'room_id' => 'lux', 'date' => '2024-10-04'],
    ['hotel_id' => 'reddison', 'room_id' => 'lux', 'date' => '2024-10-05'],
] as $row)
{
    $repository->set(
        key: RoomAvailability::makeKey($row['hotel_id'], $row['room_id'], new \DateTime($row['date'])),
        value: $row
    );
}

return $repository;