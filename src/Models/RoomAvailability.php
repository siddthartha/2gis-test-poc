<?php
declare(strict_types=1);

namespace App\Models;

use Swoole\Table;

class RoomAvailability implements AvailabilityInterface
{

    public function __construct(private Table &$repository)
    {
    }

    public static function makeKey(string $hotelId, string $roomId, \DateTime $date): string
    {
        return "{$hotelId}_{$roomId}_" . $date->format('Y-m-d');
    }

    public function daysBetween(\DateTime $from, \DateTime $to): array
    {
        if ($from > $to)
        {
            return [];
        }

        $days = [];

        $period = new \DatePeriod(
            $from,
            new \DateInterval('P1D'),
            (clone $to)->modify('+1 day')
        );

        foreach ($period as $day)
        {
            $days[] = $day;
        }

        return $days;
    }

    public function isDateAvailable(string $hotelId, string $roomId, \DateTime $date): bool
    {
        if ($this->repository->get(
            $this->makeKey($hotelId, $roomId, $date)
        ) == null) {
            return true;
        }

        return false;
    }

    public function bookRoom(Order $order): void
    {
        $daysToBook = $this->daysBetween(
            from: new \DateTime($order->getAttribute('from')),
            to: new \DateTime($order->getAttribute('to')),
        );

        foreach ($daysToBook as &$day)
        {
            $this->repository->set(
                key: $this->makeKey(
                    hotelId: $order->getAttribute('hotel_id'),
                    roomId: $order->getAttribute('room_id'),
                    date: $day
                ),
                value: [
                    'hotel_id' => $order->getAttribute('hotel_id'),
                    'room_id' => $order->getAttribute('room_id'),
                    'date' => $day->format('Y-m-d'),
                ]
            );
        }
    }

    public function isPeriodAvailable(string $hotelId, string $roomId, \DateTime $dateFrom, \DateTime $dateTo): array
    {
        $unavailableDays = [];

        foreach ($this->daysBetween(
            from: $dateFrom,
            to: $dateTo,
        ) as $day)
        {
            if (!$this->isDateAvailable(
                hotelId: $hotelId,
                roomId: $roomId,
                date: $day,
            )) {
                $unavailableDays[] = $day;
            }
        }

        return $unavailableDays;
    }
}
