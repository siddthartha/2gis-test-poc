<?php

namespace App\Models;

interface AvailabilityInterface
{
    public function isDateAvailable(string $hotelId, string $roomId, \DateTime $date): bool;
    public function bookRoom(Order $order): void;
}