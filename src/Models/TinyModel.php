<?php

namespace App\Models;

class TinyModel implements \JsonSerializable
{
    protected array $attributes;

    public function rules(): array
    {
        return [];
    }

    public function fields(): array
    {
        return [];
    }

    public function output(): array
    {
        $output = $this->fields();

        array_map(
            function ($key, $value) use (&$output)
            {
                if (is_int($key) && $this->hasAttribute($value))
                {
                    $realValue = $this->getAttribute($value);
                    $output[$value] = $realValue;
                    unset($output[$key]);

                    return $output[$value];
                }

                if ($value instanceof \Closure)
                {
                    return $output[$key] = $value($this);
                }

                if ($this->hasAttribute($value))
                {
                    return $output[$key] = $this->getAttribute($value);
                }

                return $value;
            },
            array_keys($output),
            array_values($output)
        );

        return $output;
    }

    public function __construct(array $data)
    {
        $this->fill($data);

        return $this;
    }

    public function fill($data): self
    {
        $this->validate($data, $this->rules());

        foreach ($data as $attribute => $value)
        {
            $this->setAttribute($attribute, $value);
        }

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function validate(array $data, array $rules): void
    {
        foreach ($data as $attribute => $value)
        {
            $rule = $rules[$attribute];
            $failMessage = 'Model ' . static::class . ' field "' . $attribute . '" validation failed';

            if (
                $rule instanceof \Closure
                && $rule($value) == false
            )
            {
                throw new \Exception($failMessage);
            }

            match ($rule)
            {
                'int' => !is_int($value) ? throw new \Exception($failMessage) : null,
                default => null,
            };
        }

        if (array_keys($data) !== array_keys($rules))
        {
            throw new \Exception("Missing required fields [" . implode(', ', array_diff(array_keys($rules), array_keys($data))) . '] in model ' . static::class);
        }
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function getAttribute(string $key, $default = null): mixed
    {
        return $this->attributes[$key]
            ?? $default;
    }

    public function setAttribute(string $key, mixed $value): mixed
    {
        return
            ($this->attributes[$key] = $value);
    }

    public function hasAttribute($value): bool
    {
        return
            isset($this->attributes[$value]);
    }

    public function jsonSerialize(): array
    {
        return $this->output();
    }
}
