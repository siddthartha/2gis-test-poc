<?php
declare(strict_types=1);

namespace App\Models;

class Order extends TinyModel
{
    function validateDate(string $date, string $format = 'Y-m-d'): bool
    {
        $d = \DateTime::createFromFormat($format, $date);

        return $d
            && ($d->format($format) === $date)
            && $d > new \DateTime('now');
    }

    public function rules(): array
    {
        return [
            'hotel_id' => "string",
            'room_id' => "string",
            'email' => fn ($v) => filter_var($v, FILTER_VALIDATE_EMAIL) !== false,
            'from' => fn ($v) => $this->validateDate($v),
            'to' => fn ($v) => $this->validateDate($v),
        ];
    }

    public function fields(): array
    {
        return [
            'hotel_id' => (fn (Order &$o) => $o->getAttribute('hotel_id')),
            'room_id',
            'email',
            'from',
            'to',
        ];
    }
}
