<?php
declare(strict_types=1);

require 'vendor/autoload.php';

use App\Controllers\OrderController;
use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;

$server = new Server("0.0.0.0", 80);

$repository = require_once("Db/Repository.php");

$server->on("start", function (Server $server)
{
    echo "Swoole HTTP server is started at http://0.0.0.0:80\n";
});

$server->on("request", function (Request $request, Response $response) use (&$repository)
{
    $response->header("Access-Control-Allow-Origin", "none");

    echo "REQUEST: " . $request->rawContent() . "\n";

    $uri = $request->server['request_uri'];
    $method = $request->server['request_method'];

    if ($uri === '/orders' && $method === 'POST')
    {
        $controller = new OrderController($repository);
        $controller->createOrder($request, $response);

        echo "STATE: " . json_encode($repository) . "\n";
    }
    else
    {
        $response->status(404);
        $response->end('Not Found');
    }
});

$server->start();
