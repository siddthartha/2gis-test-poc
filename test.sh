#!/bin/bash

curl -s --location --request POST 'localhost:80/orders' \
--header 'Content-Type: application/json' \
--data-raw '{
    "hotel_id": "reddison",
    "room_id": "lux",
    "email": "guest@mail.ru",
    "from": "2024-11-01",
    "to": "2024-11-03"
}'
